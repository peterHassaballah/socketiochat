# MongoChat

Simple chat app that uses MongoDB and Socket.io
Socket .io is used to create a connection between the users ,
while mongodb is used to save the chat history

### Version
1.0.0

## Install Dependencies
```bash
npm install 
```
## Steps Before Running The App
```bash
Go the server.js file and in line 11 enter your database uri
```

## Run Server
```bash
npm start
```

## Run App
Open index.html
